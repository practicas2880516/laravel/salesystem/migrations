# Migraciones

Este proyecto se encontraran las migraciones de base de datos para el funcionamiento del proyecto RH Constructores.

## Configuraciones previas
- Copia el archivo "liquibase.properties.example" y ponle el nombre de "liquibase.properties".

## Ejecución de las migraciones
Para poder ejecutar las migraciones a la base de datos, se debe realizar el siguiente comando:
* Docker


    docker run --workdir="/project" --rm -v $PWD:/project --network=savne-network liquibase/liquibase --defaultsFile=liquibase.properties update


* Windows


    liquibase update --changelog-file=./changelog.xml


## Rollback de las migraciones
Para poder realizar rollback a las migraciones, se debe realizar el siguiente comando:
*  Docker


    docker run --workdir="/project" --rm -v $PWD:/project --network=savne-network liquibase/liquibase --defaultsFile=liquibase.properties rollback-count --count=1


* Windows


    liquibase rollback-count --changelog-file=./changelog.xml --count=1


- Siendo count el número de migraciones a las cuales se requiere hacer rollback
